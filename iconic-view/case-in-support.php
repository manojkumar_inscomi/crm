<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>CRM ADMIN PANEL</title>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="css/material-design-iconic-font.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/animate.css">
    <link type="text/css" rel="stylesheet" href="css/layout.css">
    <link type="text/css" rel="stylesheet" href="css/components.css">
    <link type="text/css" rel="stylesheet" href="css/widgets.css">
    <link type="text/css" rel="stylesheet" href="css/plugins.css">
    <link type="text/css" rel="stylesheet" href="css/pages.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap-extend.css">
    <link type="text/css" rel="stylesheet" href="css/common.css">
    <link type="text/css" rel="stylesheet" href="css/responsive.css">
    <link type="text/css" id="themes" rel="stylesheet" href="#">
</head>

<body class="leftbar-view iconic-view">
    <!--Topbar Start Here-->
    <header class="topbar clearfix">
        <!--Top Search Bar Start Here-->
        <div class="top-search-bar">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="search-input-addon">
                            <span class="addon-icon"><i class="zmdi zmdi-search"></i></span>
                            <input type="text" class="form-control top-search-input" placeholder="Search">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Top Search Bar End Here-->
        <!--Topbar Left Branding With Logo Start-->
        <div class="topbar-left pull-left">
            <div class="clearfix">
                <ul class="left-branding pull-left clickablemenu ttmenu dark-style menu-color-gradient">
                    <li><span class="left-toggle-switch"><i class="zmdi zmdi-menu"></i></span></li>
                    <li>
                        <div class="logo">
                            <a href="index.php" title="Admin Template"><img src="images/logo.png" alt="logo"></a>
                        </div>
                    </li>
                </ul>
                <!--Mobile Search and Rightbar Toggle-->
                <ul class="branding-right pull-right">
                    <li><a href="#" class="btn-mobile-search btn-top-search"><i class="zmdi zmdi-search"></i></a></li>
                    <li><a href="#" class="btn-mobile-bar"><i class="zmdi zmdi-menu"></i></a></li>
                </ul>
            </div>
        </div>
        <!--Topbar Left Branding With Logo End-->
        <!--Topbar Right Start-->
        <div class="topbar-right pull-right">
            <div class="clearfix">
                <!--Mobile View Leftbar Toggle-->
                <ul class="left-bar-switch pull-left">
                    <li><span class="left-toggle-switch"><i class="zmdi zmdi-menu"></i></span></li>
                </ul>
                <ul class="pull-right top-right-icons">
                    <li class="dropdown apps-dropdown">
                        <a href="#" class="btn-apps dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-apps"></i></a>
                        <div class="dropdown-menu">
                            <ul class="apps-shortcut clearfix">
                                <li>
                                    <a href="#"><i class="zmdi zmdi-email"></i>
                                        <span class="apps-noty">23</span>
                                        <span class="apps-label">Email</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#"><i class="zmdi zmdi-accounts-alt"></i>
                                        <span class="apps-noty">15</span>
                                        <span class="apps-label">Forum</span>
                                    </a>
                                </li>
                            </ul>
                            <ul class="more-apps">
                                <li><a href="#"><i class="zmdi zmdi-comments"></i> Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        <!--Topbar Right End-->
    </header>
    <!--Topbar End Here-->

    <!--Leftbar Start Here-->
    <aside class="iconic-leftbar">
        <div class="iconic-aside-container">
            <div class="user-profile-container">
                <div class="user-profile clearfix">
                    <div class="admin-user-thumb">
                        <img src="images/avatar/user-7.png" alt="admin">
                    </div>
                    <div class="admin-user-info">
                        <ul>
                            <li><a href="#">Kamrujaman Shohel</a></li>
                            <li><a href="#">Info@jaman.me</a></li>
                        </ul>
                    </div>
                </div>
                <div class="admin-bar">
                    <ul>
                        <li><a href="#"><i class="zmdi zmdi-power"></i>
                            </a>
                        </li>
                        <li><a href="#"><i class="zmdi zmdi-account"></i>
                            </a>
                        </li>
                        <li><a href="#"><i class="zmdi zmdi-key"></i>
                            </a>
                        </li>
                        <li><a href="#"><i class="zmdi zmdi-settings"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <ul class="list-accordion">
                <li>
                    <a href="#"><i class="zmdi zmdi-view-dashboard"></i><span class="list-label">Master</span></a>
                    <ul>
                        <li><a href="#">Product</a>
                            <ul>
                                <li><a href="addproduct.html">Add Product</a></li>
                                <li><a href="#">View Product</a></li>
                            </ul>
                        </li>

                        <li><a href="#">Customer</a>
                            <ul>
                                <li><a href="add_organization.php">Add Customer</a></li>
                                <li><a href="#">View Customer</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Vendor</a>
                            <ul>
                                <li><a href="">Add Vendor</a></li>
                                <li><a href="#">View Vendor</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="#"><i class="zmdi zmdi-view-web"></i><span class="list-label">Sales</span></a>
                    <ul>
                        <li><a href="#">Opportunites</a>
                            <ul>
                                <li><a href="addopportunity.php">Add Opportunites</a></li>
                                <li><a href="view_opportunity.php">View Opportunites</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Quotes</a>
                            <ul>
                                <li><a href="sale_qutescreen.php">Add Quotes</a></li>
                                <li><a href="#">View Quotes</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Forecast and Quotes</a>
                            <ul>
                                <li><a href="forcast.php">Forecast</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Sale Order</a>
                            <ul>
                                <li><a href="saleoerpage.php">Add Order</a></li>
                                <li><a href="#">View Order</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Sale Invoice</a>
                            <ul>
                                <li><a href="invoices.php">Add Invoice</a></li>
                                <li><a href="#">View Invoice</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="zmdi zmdi-check"></i><span class="list-label">Support</span></a>
                    <ul>
                        <li><a href="#">Case</a>
                            <ul>
                                <li><a href="caseinsupport.php">Add Case</a></li>
                                <li><a href="#">View Case</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Support</a>
                            <ul>
                                <li><a href="">Support Insights</a></li>
                                <li><a href="#">View Case</a></li>
                            </ul>
                        </li>
                        <li><a href="#">FAQ</a>
                            <ul>
                                <li><a href="addfaq.php">Add Faq</a></li>
                                <li><a href="">View Faq</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Assets</a>
                            <ul>
                                <li><a href="assetadd.php">Add Assets</a></li>
                                <li><a href="">View Assets</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Service Contracts</a>
                            <ul>
                                <li><a href="service.php">Add Service</a></li>
                                <li><a href="">View Service</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="zmdi zmdi-swap"></i><span class="list-label">Inventory</span></a>
                    <ul>
                        <li><a href="#">Inventory</a>
                            <ul>
                                <li><a href="addproduct.php">Add Product</a></li>
                                <li><a href="#">View Product</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Service</a>
                            <ul>
                                <li><a href="service.php">Add Service</a></li>
                                <li><a href="servicereviewscreen.php">View Service</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Invoice</a>
                            <ul>
                                <li><a href="invoices.php">Add Invoice</a></li>
                                <li><a href="">View Invoice</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Price Book</a>
                            <ul>
                                <li><a href="pricebook.php">Add Price Book</a></li>
                                <li><a href="">View Price Book</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Purchase Order</a>
                            <ul>
                                <li><a href="purchaseorder.php">Add Purchase</a></li>
                                <li><a href="#">View Purchase</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="zmdi zmdi-grid"></i><span class="list-label">Projects</span></a>
                    <ul>
                        <li><a href="#">Projects</a>
                            <ul>
                                <li><a href="project.php">Add Projects</a></li>
                                <li><a href="project_list">View Projects</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Projects Task</a>
                            <ul>
                                <li><a href="projecttask.php">Add Projects Task</a></li>
                                <li><a href="project_task_list">View Projects Task</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Project Milestones</a>
                            <ul>
                                <li><a href="projectmilestone.php">Add Milestones</a></li>
                                <li><a href="#">View Milestones</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="zmdi zmdi-chart"></i><span class="list-label">Requirement</span></a>
                    <ul>
                        <li><a href="#">Requistion</a>
                            <ul>
                                <li><a href="recruitment_request">New Request</a></li>
                                <li><a href="recruitment_request_list">View Request</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Request Recruitment</a>
                            <ul>
                                <li><a href="profiling">Profiling</a></li>
                                <li><a href="candidates_list">Candidates List</a></li>
                                <li><a href="cv_upload">CV Addition</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Interview</a>
                            <ul>
                                <li><a href="interview_panel">Interview Panel</a></li>
                                <li><a href="schedule_interview">Schedule Interview</a></li>
                                <li><a href="scheduled_interview_list">Candidate Feedback</a></li>
                                <li><a href="selected_candidate_list">Document Verification</a></li>
                                <li><a href="offer_roll_out">Offer Roll Out</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <li>
                    <a href="#"><i class="zmdi zmdi-apps"></i><span class="list-label">Employee Management</span></a>
                    <ul>
                        <li><a href="#">Employee Onboarding</a>
                            <ul>
                                <li><a href="add_employee">New Employee</a></li>
                                <li><a href="employee_list">View Employees</a></li>
                                <li><a href="asset_allocation">Asset Allocation</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Leave</a>
                            <ul>
                                <li><a href="leave_employee">Leave</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Employee Relation</a>
                            <ul>
                                <li><a href="issue_management">Issue Management</a></li>
                                <li><a href="issues_list">Issues List</a></li>
                                <li><a href="request_manager">Request Manager</a></li>
                                <li><a href="request_list">Request List</a></li>
                                <li><a href="loans">Loans / Advances</a></li>
                                <li><a href="loan_list">Loan List</a></li>
                            </ul>
                        </li>

                        <li><a href="#">Insurance</a>
                            <ul>
                                <li><a href="claim_insurance">Claim Insurance (New)</a></li>
                                <li><a href="insurance_list">View Insurance</a></li>
                            </ul>
                        </li>
                        <li><a href="#">IT Deduction</a>
                            <ul>
                                <li><a href="it_declaration">IT Declaration</a></li>
                                <li><a href="it_declaration_list">IT List</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Employee Attendance</a>
                            <ul>
                                <li><a href="#">Attendance</a></li>
                                <li><a href="#">View Attendance</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Employee Payroll</a>
                            <ul>
                                <li><a href="employee_salary">Add Payslip</a></li>
                                <li><a href="#">List Payments</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Seperation</a>
                            <ul>
                                <li><a href="initiate_resignation">Initiate Resignation</a></li>
                                <li><a href="resignation_acceptance">Resignation Acceptance</a></li>
                                <li><a href="final_settlement">Full & Final Settlement</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#"><i class="zmdi zmdi-view-dashboard"></i><span class="list-label">Performance</span></a>
                    <ul>
                        <li><a href="#">Performance Management</a>
                            <ul>
                                <li><a href="set_goals">Set Goals</a></li>
                                <li><a href="update_performance">Update Performance</a></li>
                                <li><a href="performance_review">Performance Review</a></li>
                                <li><a href="#">History Information</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </aside>
    <!--Leftbar End Here-->
    <!--Page Container Start Here-->
    <section class="main-container">
        <div class="container-fluid">
            <div class="page-header filled full-block light">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <h2>Case in Support</h2>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <ul class="list-page-breadcrumb">
                            <li><a href="#">Home <i class="zmdi zmdi-chevron-right"></i></a></li>
                            <li><a href="#">Support <i class="zmdi zmdi-chevron-right"></i></a></li>
                            <li><a href="#">Case <i class="zmdi zmdi-chevron-right"></i></a></li>
                            <li class="active-page">Add Case</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="widget-wrap">

                        <div class="widget-container margin-top-0 clearfix">
                            <div class="widget-content">
                                <form action=""
                                    method="post" class="j-forms" id="j-forms-validation" novalidate>

                                    <div class="form-content">

                                        <!-- start one field form group -->
                                        <div class="col-md-12 unit">

                                            <h3>Case Summary Information</h3>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-1">
                                                <label class="label">summary</label></div>
                                            <div class="col-md-11">
                                                <div class="input">
                                                    <textarea spellcheck="false" name="casesumm" id="casesumm" class="form-control "></textarea>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="clearfix">
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                        <div class="col-md-12 unit">

                                            <h3>Case Information</h3>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Case Title</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="input">

                                                        <input class="form-control required-group" type="text" name="casetitle"
                                                            id="casetitle">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Status</label></div>
                                                <div class="col-md-9">
                                                    <label class="input select">
                                                        <select name="casestatus" id="casestatus" class="form-control">
                                                            <option value="none" selected="" disabled=""> select an
                                                                option</option>
                                                            <option value="New">New</option>
                                                            <option value="Assigned">Assigned</option>
                                                            <option value="Open">Open</option>
                                                            <option value="Wait for Customer">Wait for Customer</option>
                                                            <option value="Wait for 3rd party">Wait for 3rd party</option>
                                                            <option value="Resolved">Resolved</option>
                                                            <option value="Closed">Closed</option>

                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Priority</label></div>
                                                <div class="col-md-9">
                                                    <label class="input select">
                                                        <select name="casepriority" id="casepriority" class="form-control">
                                                            <option value="none" selected="" disabled=""> select an
                                                                option</option>
                                                            <option value="Urgent">Urgent</option>
                                                            <option value="High">High</option>
                                                            <option value="Medium">Medium</option>
                                                            <option value="Low">Low</option>

                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">contact Name</label></div>
                                                <div class="col-md-9">
                                                    <div class="input">
                                                        <input class="form-control" type="search" placeholder="Type to search..."
                                                            id="casecname" name="casecname"><label class="icon-left"
                                                            for="casecname">
                                                            <i class="fa fa-search"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                        <div class="row">

                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Organization Name</label></div>
                                                <div class="col-md-9">
                                                    <div class="input">
                                                        <input class="form-control" type="search" placeholder="Type to search..."
                                                            id="caseoname" name="caseoname"><label class="icon-left"
                                                            for="caseoname">
                                                            <i class="fa fa-search"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Group</label></div>
                                                <div class="col-md-9">
                                                    <label class="input select">
                                                        <select name="group" id = "groupid" class="form-control">
                                                            <option value="none" selected="" disabled=""> select an
                                                                option</option>
                                                            <option value="Marketing Group">Marketing Group</option>
                                                            <option value="Support Group">Support Group</option>
                                                            <option value="Team Selling">Team Selling</option>

                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>



                                        <div class="row">
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Assigned To</label></div>
                                                <div class="col-md-9">
                                                    <label class="input select">
                                                        <select name="caseassigned" id="caseassigned" class="form-control">
                                                            <option value="none" selected="" disabled="">Users</option>
                                                            <option value="name"></option>
                                                            <option value="none" selected="" disabled="">Groups</option>
                                                            <option value="Marketing group">Marketing group</option>
                                                            <option value="Support groups">Support group</option>
                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Product Name</label></div>
                                                <div class="col-md-9">
                                                    <div class="input">
                                                        <input class="form-control" type="search" placeholder="Type to search..."
                                                            id="casepname" name="casepname"><label class="icon-left"
                                                            for="casepname">
                                                            <i class="fa fa-search"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Channel</label></div>
                                                <div class="col-md-9">
                                                    <label class="input select">
                                                        <select name="casechen" id="casechen" class="form-control">
                                                            <option value="none" selected="" disabled=""> select an
                                                                option</option>
                                                            <option value="Email">Email</option>
                                                            <option value="Phone">Phone</option>
                                                            <option value="Chat">Chat</option>
                                                            <option value="Social">Social</option>
                                                            <option value="Webform">Webform</option>

                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="col-md-2">
                                                    <label class="label">Resolution</label></div>
                                                <div class="col-md-10">
                                                    <div class="input">
                                                        <textarea spellcheck="false" name="caseresol" id="caseresol"
                                                            class="form-control"></textarea>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                        <br>

                                        <div class="row">

                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Category</label></div>
                                                <div class="col-md-9">
                                                    <label class="input select">
                                                        <select name="casecategory" id="casecategory" class="form-control">
                                                            <option value="none" selected="" disabled=""> select an
                                                                option</option>
                                                            <option value="Sales">Sales</option>
                                                            <option value="How to">How to</option>
                                                            <option value="Billing">Billing</option>
                                                            <option value="Defect">Defect</option>
                                                            <option value="Enhancement">Enhancement</option>
                                                            <option value="IT">IT</option>
                                                            <option value="Others">Others</option>

                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </div>
                                            </div>

                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Sub Category</label></div>
                                                <div class="col-md-9">
                                                    <label class="input select">
                                                        <select name="casesubcategory" id="casesubcategory" class="form-control">
                                                            <option value="none" selected="" disabled=""> select an
                                                                option</option>
                                                            <option value="Pricing">Pricing</option>
                                                            <option value="Discount">Discount</option>
                                                            <option value="Ordering">Ordering</option>
                                                            <option value="Installation">Installation</option>
                                                            <option value="Assembly">Assembly</option>
                                                            <option value="Usage">Usage</option>
                                                            <option value="Dispute">Dispute</option>
                                                            <option value="Mode">Mode</option>
                                                            <option value="Enquiry">Enquiry</option>
                                                            <option value="Cancellation">Cancellation</option>
                                                            <option value="Returns and Exchange">Returns and Exchange</option>
                                                            <option value="Functional">Functional</option>
                                                            <option value="Damage">Damage</option>
                                                            <option value="Performance">Performance</option>
                                                            <option value="Usability">Usability</option>
                                                            <option value="Login">Login</option>
                                                            <option value="Order Fulfilment">Order Fulfilment</option>
                                                            <option value="Career">Career</option>
                                                            <option value="Marketing">Marketing</option>
                                                            <option value="Partner">Partner</option>
                                                            <option value="Spam">Spam</option>


                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">


                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Resolution Type</label></div>
                                                <div class="col-md-9">
                                                    <label class="input select">
                                                        <select name="casertype" id="casertype" class="form-control">
                                                            <option value="none" selected="" disabled=""> select an
                                                                option</option>
                                                            <option value="Answered">Answered</option>
                                                            <option value="Fixed">Fixed</option>
                                                            <option value="Wont Fix">Wont Fix</option>
                                                            <option value="Planned">Planned</option>
                                                            <option value="Did not respond">Did not respond</option>
                                                            <option value="Duplicate">Duplicate</option>
                                                            <option value="Spam">Spam</option>
                                                            <option value="Others">Others</option>

                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Deferred Date</label>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="input">

                                                        <input class="form-control required-group" type="date" name="casedefdate"
                                                            id="casedefdate">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Service Contracts</label></div>
                                                <div class="col-md-9">
                                                    <div class="input">
                                                        <input class="form-control" type="search" placeholder="Type to search..."
                                                            id="casescon" name="casescon"><label class="icon-left" for="casescon">
                                                            <i class="fa fa-search"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 unit">
                                                <div class="col-md-3">
                                                    <label class="label">Asset</label></div>
                                                <div class="col-md-9">
                                                    <div class="input">
                                                        <input class="form-control" type="search" placeholder="Type to search..."
                                                            id="caseasset" name="caseasset"><label class="icon-left"
                                                            for="caseasset">
                                                            <i class="fa fa-search"></i>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                    </div>
                                    <div class="clearfix">
                                    </div>
                                    <div class="clearfix">
                                    </div>
                                    <div class="col-md-12 unit">

                                        <h3>SLA Information</h3>

                                    </div>

                                    <div class="row">

                                        <div class="col-md-6 unit">
                                            <div class="col-md-3">
                                                <label class="label">SLA name</label></div>
                                            <div class="col-md-9">
                                                <div class="input">
                                                    <input class="form-control" type="search" placeholder="Type to search..."
                                                        id="casesla" name="casesla"><label class="icon-left" for="casesla">
                                                        <i class="fa fa-search"></i>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12 unit">

                                        <h3>Service Details</h3>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 unit">
                                            <div class="col-md-3">
                                                <label class="label">Is billable?</label>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="input">

                                                    <input class="form-control required-group" type="checkbox" name="casebillable"
                                                        id="casebillable">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 unit">
                                            <div class="col-md-3">
                                                <label class="label">Service Type</label></div>
                                            <div class="col-md-9">
                                                <label class="input select">
                                                    <select name="rtype" id = "servicetypeid" class="form-control">
                                                        <option value="none" selected="" disabled=""> select an option</option>
                                                        <option value="Scheduled Maintainance">Scheduled Maintainance</option>
                                                        <option value="Service Request">Service Request</option>
                                                        <option value="Incident">Incident</option>
                                                        <option value="Implementation">Implementation</option>



                                                    </select>
                                                    <i></i>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">

                                        <div class="col-md-6 unit">
                                            <div class="col-md-3">
                                                <label class="label">Service Location</label></div>
                                            <div class="col-md-9">
                                                <label class="input select">
                                                    <select name="casesltype" id="casesltype" class="form-control">
                                                        <option value="none" selected="" disabled=""> select an option</option>
                                                        <option value="Remote">Remote</option>
                                                        <option value="Onsite">Onsite</option>



                                                    </select>
                                                    <i></i>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-3">
                                                <label class="label">Work Location</label></div>
                                            <div class="col-md-9">
                                                <div class="input">
                                                    <textarea spellcheck="false" name="casewloc" id="casewloc" class="form-control "></textarea>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-actions text-center">
                                            <button type="button" class="btn btn-default">Cancel</button>

                                            <button type="submit" name = "submit" id = "submitid" class="btn btn-primary">Save changes</button>

                                        </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!--Footer Start Here -->
        <footer class="footer-container">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <div class="footer-left">
                            <span>© 2019 <a href="http://inscomi.com">inscomi</a></span>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="footer-right">
                            <span class="footer-meta">Crafted with&nbsp;<i class="fa fa-heart"></i>&nbsp;by&nbsp;<a
                                    href="http://inscomi.com">inscomi</a></span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--Footer End Here -->
    </section>
    <!--Page Container End Here-->
    <!--Rightbar Start Here-->
    <!--Rightbar End Here-->
    <script src="js/lib/jquery.js"></script>
    <script src="js/lib/jquery-migrate.js"></script>
    <script src="js/lib/bootstrap.js"></script>
    <script src="js/lib/jquery.ui.js"></script>
    <script src="js/lib/jRespond.js"></script>
    <script src="js/lib/nav.accordion.js"></script>
    <script src="js/lib/hover.intent.js"></script>
    <script src="js/lib/hammerjs.js"></script>
    <script src="js/lib/jquery.hammer.js"></script>
    <script src="js/lib/jquery.fitvids.js"></script>
    <script src="js/lib/scrollup.js"></script>
    <script src="js/lib/smoothscroll.js"></script>
    <script src="js/lib/jquery.slimscroll.js"></script>
    <script src="js/lib/jquery.syntaxhighlighter.js"></script>
    <script src="js/lib/velocity.js"></script>
    <script src="js/lib/jquery-jvectormap.js"></script>
    <script src="js/lib/jquery-jvectormap-world-mill.js"></script>
    <script src="js/lib/jquery-jvectormap-us-aea.js"></script>
    <script src="js/lib/smart-resize.js"></script>
    <!--iCheck-->
    <script src="js/lib/icheck.js"></script>
    <script src="js/lib/jquery.switch.button.js"></script>
    <!--CHARTS-->
    <script src="js/lib/chart/sparkline/jquery.sparkline.js"></script>
    <script src="js/lib/chart/easypie/jquery.easypiechart.min.js"></script>
    <script src="js/lib/chart/flot/excanvas.min.js"></script>
    <script src="js/lib/chart/flot/jquery.flot.min.js"></script>
    <script src="js/lib/chart/flot/curvedLines.js"></script>
    <script src="js/lib/chart/flot/jquery.flot.time.min.js"></script>
    <script src="js/lib/chart/flot/jquery.flot.stack.min.js"></script>
    <script src="js/lib/chart/flot/jquery.flot.axislabels.js"></script>
    <script src="js/lib/chart/flot/jquery.flot.resize.min.js"></script>
    <script src="js/lib/chart/flot/jquery.flot.tooltip.min.js"></script>
    <script src="js/lib/chart/flot/jquery.flot.spline.js"></script>
    <script src="js/lib/chart/flot/jquery.flot.pie.min.js"></script>
    <!--Forms-->
    <script src="js/lib/jquery.maskedinput.js"></script>
    <script src="js/lib/jquery.validate.js"></script>
    <script src="js/lib/jquery.form.js"></script>
    <script src="js/lib/j-forms.js"></script>
    <script src="js/lib/jquery.loadmask.js"></script>
    <script src="js/lib/vmap.init.js"></script>
    <script src="js/lib/theme-switcher.js"></script>
    <script src="js/apps.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        $('#submitid').on('click', function() {
            event.preventDefault();
            var summary = $("#casesumm").val();
            var casetitle =  $("#casetitle").val();
            var status = $("#casestatus").val();
            var priority = $("#casepriority").val();
            var contactname = $("#casecname").val();
            var organizationname = $("#caseoname").val();
            var group = $("#groupid").val();
            var assignedto = $("#caseassigned").val();
            var productname = $("#casepname").val();
            var channel = $("#casechen").val();
            var resolution = $("#caseresol").val();
            var category = $("#casecategory").val();
            var subcategory = $("#casesubcategory").val();
            var resolutiontype = $("#casertype").val();
            var deffereddate = $("#casedefdate").val();
            var servicecontracts = $("#faqcategory").val();
            var asset = $("#caseasset").val();
            var slaname = $("#casesla").val();
            var isbillable = $("#casebillable").val();
            var servicetype = $("#servicetypeid").val();
            var servicelocation = $("#casesltype").val();
            var worklocation = $("#casewloc").val();
            
            var dataString = 'Summary=' + summary + '&CaseTitle=' + casetilte + '&Status=' + status + '&Priority ='
            + priority + '&ContactName=' + contactname + '&OrganizationName=' + organizationname  + '&Group=' + group 
            + '&AssignedTo=' + assignedto + '&Channel=' + channel +  '&Resolution=' + resolution +  '&Category=' + category 
            +  '&SubCategory=' + subcategory +  '&ResolutionType=' + resolutiontype +  '&DefferedDate=' + deffereddate 
            +  '&ServiceContracts=' + servicecontracts +  '&Asset=' + asset +  '&SlaName=' + slaname +  '&IsBillable=' + isbillable 
            +  '&ServiceType=' + servicetype +  '&ServiceLocation=' + servicelocation +  '&Worklocation=' + worklocation ;
        
        alert(dataString); 

            if (summary == '' || casetilte == '' || status == '' || priority == '' || contactname == '' || organizationname  == ''
            || group == ''|| assignedto == '' || channel == '' || resolution == '' || category  == ''
            || subcategory == '' || resolutiontype == '' || deffereddate == '' || servicecontracts == '' || asset == '' 
            || slaname == '' || isbillable == '' || servicetype == '' || servicelocation == '' || worklocation == '' )
            
            
            {
                alert("Please Fill All Fields");
            }
            else
            {
                        $.ajax({
                        type: "POST",
                        url: './controller/addfaq.php',
                        data: dataString,
                        
                        beforeSend: function(){
                            alert(dataString);   
                        },
                        success: function() {
                        alert("sucess");
                        },
                        error : function () {
                            alert("failed");
                        }
                        });
            }
          return false;
        });
    
    });
    </script>
</body>

</html>