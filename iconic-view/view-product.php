<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>CRM ADMIN PANEL</title>
    <link type="text/css" rel="stylesheet" href="css/font-awesome.css">
    <link type="text/css" rel="stylesheet" href="css/material-design-iconic-font.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap.css">
    <link type="text/css" rel="stylesheet" href="css/animate.css">
    <link type="text/css" rel="stylesheet" href="css/layout.css">
    <link type="text/css" rel="stylesheet" href="css/components.css">
    <link type="text/css" rel="stylesheet" href="css/widgets.css">
    <link type="text/css" rel="stylesheet" href="css/plugins.css">
    <link type="text/css" rel="stylesheet" href="css/pages.css">
    <link type="text/css" rel="stylesheet" href="css/bootstrap-extend.css">
    <link type="text/css" rel="stylesheet" href="css/common.css">
    <link type="text/css" rel="stylesheet" href="css/responsive.css">
    <link type="text/css" id="themes" rel="stylesheet" href="#">
</head>
<body class="leftbar-view iconic-view">
<!--Topbar Start Here-->
<header class="topbar clearfix">
    <!--Top Search Bar Start Here-->
    <div class="top-search-bar">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="search-input-addon">
                        <span class="addon-icon"><i class="zmdi zmdi-search"></i></span>
                        <input type="text" class="form-control top-search-input" placeholder="Search">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Top Search Bar End Here-->
    <!--Topbar Left Branding With Logo Start-->
    <div class="topbar-left pull-left">
        <div class="clearfix">
            <ul class="left-branding pull-left clickablemenu ttmenu dark-style menu-color-gradient">
                <li><span class="left-toggle-switch"><i class="zmdi zmdi-menu"></i></span></li>
                <li>
                    <div class="logo">
                        <a href="index.php" title="Admin Template"><img src="images/logo.png" alt="logo"></a>
                    </div>
                </li>
            </ul>
            <!--Mobile Search and Rightbar Toggle-->
            <ul class="branding-right pull-right">
                <li><a href="#" class="btn-mobile-search btn-top-search"><i class="zmdi zmdi-search"></i></a></li>
                <li><a href="#" class="btn-mobile-bar"><i class="zmdi zmdi-menu"></i></a></li>
            </ul>
        </div>
    </div>
    <!--Topbar Left Branding With Logo End-->
    <!--Topbar Right Start-->
    <div class="topbar-right pull-right">
        <div class="clearfix">
            <!--Mobile View Leftbar Toggle-->
            <ul class="left-bar-switch pull-left">
                <li><span class="left-toggle-switch"><i class="zmdi zmdi-menu"></i></span></li>
            </ul>
            <ul class="pull-right top-right-icons">
                <li class="dropdown apps-dropdown">
                    <a href="#" class="btn-apps dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-apps"></i></a>
                    <div class="dropdown-menu">
                        <ul class="apps-shortcut clearfix">
                            <li>
                                <a href="#"><i class="zmdi zmdi-email"></i>
                                    <span class="apps-noty">23</span>
                                    <span class="apps-label">Email</span>
                                </a>
                            </li>
                            <li>
                                <a href="#"><i class="zmdi zmdi-accounts-alt"></i>
                                    <span class="apps-noty">15</span>
                                    <span class="apps-label">Forum</span>
                                </a>
                            </li>
                        </ul>
                        <ul class="more-apps">
                            <li><a href="#"><i class="zmdi zmdi-comments"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
                </ul>
        </div>
    </div>
    <!--Topbar Right End-->
</header>
<!--Topbar End Here-->

<!--Leftbar Start Here-->
<aside class="iconic-leftbar">
    <div class="iconic-aside-container">
        <div class="user-profile-container">
            <div class="user-profile clearfix">
                <div class="admin-user-thumb">
                    <img src="images/avatar/user-7.png" alt="admin">
                </div>
                <div class="admin-user-info">
                    <ul>
                        <li><a href="#">Kamrujaman Shohel</a></li>
                        <li><a href="#">Info@jaman.me</a></li>
                    </ul>
                </div>
            </div>
            <div class="admin-bar">
                <ul>
                    <li><a href="#"><i class="zmdi zmdi-power"></i>
                    </a>
                    </li>
                    <li><a href="#"><i class="zmdi zmdi-account"></i>
                    </a>
                    </li>
                    <li><a href="#"><i class="zmdi zmdi-key"></i>
                    </a>
                    </li>
                    <li><a href="#"><i class="zmdi zmdi-settings"></i>
                    </a>
                    </li>
                </ul>
            </div>
        </div>
        <ul class="list-accordion">
            <li>
                <a href="#"><i class="zmdi zmdi-view-dashboard"></i><span class="list-label">Master</span></a>
                <ul>
                    <li><a href="#">Product</a>
					<ul>
                    <li><a href="add-product.php">Add Product</a></li>
                    <li><a href="view-product.php">View Product</a></li>
                                    </ul>
					</li>
									
                    <li><a href="#">Customer</a>
					<ul>
                    <li><a href="add-customer.php">Add Customer</a></li>
                    <li><a href="view-customer.php">View Customer</a></li>
                                    </ul>
									</li>
				<li><a href="#">Vendor</a>
					<ul>
                    <li><a href="">Add Vendor</a></li>
                    <li><a href="#">View Vendor</a></li>
                                    </ul>
									</li>
              
               </ul>
            </li>
            <li>
                <a href="#"><i class="zmdi zmdi-view-web"></i><span class="list-label">Sales</span></a>
                <ul>
                    <li><a href="#">Opportunites</a>
					<ul>
					<li><a href="addopportunity.php">Add Opportunites</a></li>
                    <li><a href="view_opportunity.php">View Opportunites</a></li>
					</ul>
					</li>
                    <li><a href="#">Quotes</a>
					<ul>
					<li><a href="sale_qutescreen.php">Add Quotes</a></li>
                    <li><a href="#">View Quotes</a></li>
					</ul>
					</li>
                    <li><a href="#">Forecast and Quotes</a>
                    <ul>
                    <li><a href="forcast.php">Forecast</a></li>
                    </ul>
                    </li>
					<li><a href="#">Sale Order</a>
					<ul>
					<li><a href="saleoerpage.php">Add Order</a></li>
                    <li><a href="#">View Order</a></li>
					</ul>
					</li>
					<li><a href="#">Sale Invoice</a>
					<ul>
					<li><a href="invoices.php">Add Invoice</a></li>
                    <li><a href="#">View Invoice</a></li>
					</ul>
					</li>
               </ul>
            </li>
            <li>
                <a href="#"><i class="zmdi zmdi-check"></i><span class="list-label">Support</span></a>
                <ul>
                    <li><a href="#">Case</a>
					<ul>
					<li><a href="caseinsupport.php">Add Case</a></li>
					<li><a href="#">View Case</a></li>
					</ul></li>
					<li><a href="#">Support</a>
					<ul>
					<li><a href="">Support Insights</a></li>
					<li><a href="#">View Case</a></li>
					</ul></li>
					<li><a href="#">FAQ</a>
					<ul>
					<li><a href="addfaq.php">Add Faq</a></li>
					<li><a href="">View Faq</a></li>
					</ul></li>
						<li><a href="#">Assets</a>
					<ul>
					<li><a href="assetadd.php">Add Assets</a></li>
					<li><a href="">View Assets</a></li>
					</ul></li>
					<li><a href="#">Service Contracts</a>
					<ul>
					<li><a href="service.php">Add Service</a></li>
					<li><a href="">View Service</a></li>
					</ul></li>
              </ul>
            </li>
            <li>
                <a href="#"><i class="zmdi zmdi-swap"></i><span class="list-label">Inventory</span></a>
                <ul>
                    <li><a href="#">Inventory</a>
					<ul>
					<li><a href="addproduct.php">Add Product</a></li>
					<li><a href="#">View Product</a></li>
					</ul></li>
					<li><a href="#">Service</a>
					<ul>
					<li><a href="service.php">Add Service</a></li>
					<li><a href="servicereviewscreen.php">View Service</a></li>
					</ul></li>
					<li><a href="#">Invoice</a>
					<ul>
					<li><a href="invoices.php">Add Invoice</a></li>
					<li><a href="">View Invoice</a></li>
					</ul></li>
						<li><a href="#">Price Book</a>
					<ul>
					<li><a href="pricebook.php">Add Price Book</a></li>
					<li><a href="">View Price Book</a></li>
					</ul></li>
					<li><a href="#">Purchase Order</a>
					<ul>
					<li><a href="purchaseorder.php">Add Purchase</a></li>
					<li><a href="#">View Purchase</a></li>
					</ul></li>
              </ul>
            </li>
            <li>
                <a href="#"><i class="zmdi zmdi-grid"></i><span class="list-label">Projects</span></a>
                <ul>
                    <li><a href="#">Projects</a>
					<ul>
					<li><a href="project.php">Add Projects</a></li>
					<li><a href="project_list">View Projects</a></li>
					</ul></li>
					<li><a href="#">Projects Task</a>
					<ul>
					<li><a href="projecttask.php">Add Projects Task</a></li>
					<li><a href="project_task_list">View Projects Task</a></li>
					</ul></li>
					<li><a href="#">Project Milestones</a>
					<ul>
					<li><a href="projectmilestone.php">Add Milestones</a></li>
					<li><a href="#">View Milestones</a></li>
					</ul>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#"><i class="zmdi zmdi-chart"></i><span class="list-label">Requirement</span></a>
                <ul>
                    <li><a href="#">Requistion</a>
                    <ul>
                    <li><a href="recruitment_request">New Request</a></li>
                    <li><a href="recruitment_request_list">View Request</a></li>
                    </ul></li>
                    <li><a href="#">Request Recruitment</a>
                    <ul>
                    <li><a href="profiling">Profiling</a></li>
                    <li><a href="candidates_list">Candidates List</a></li>
                    <li><a href="cv_upload">CV Addition</a></li>
                    </ul></li>
                    <li><a href="#">Interview</a>
                    <ul>
                    <li><a href="interview_panel">Interview Panel</a></li>
                    <li><a href="schedule_interview">Schedule Interview</a></li>
                    <li><a href="scheduled_interview_list">Candidate Feedback</a></li>
                    <li><a href="selected_candidate_list">Document Verification</a></li>
                    <li><a href="offer_roll_out">Offer Roll Out</a></li>
                    </ul></li>                     
                    
              </ul>
            </li>
           <li>
                <a href="#"><i class="zmdi zmdi-apps"></i><span class="list-label">Employee Management</span></a>
                <ul>
                    <li><a href="#">Employee Onboarding</a>
                    <ul>
                    <li><a href="add_employee">New Employee</a></li>
                    <li><a href="employee_list">View Employees</a></li>
                    <li><a href="asset_allocation">Asset Allocation</a></li>
                    </ul></li>
                    <li><a href="#">Leave</a>
                    <ul>
                    <li><a href="leave_employee">Leave</a></li>
                    </ul></li>
                     <li><a href="#">Employee Relation</a>
                    <ul>
                    <li><a href="issue_management">Issue Management</a></li>
                    <li><a href="issues_list">Issues List</a></li>
                    <li><a href="request_manager">Request Manager</a></li>
                    <li><a href="request_list">Request List</a></li>
                    <li><a href="loans">Loans / Advances</a></li>
                    <li><a href="loan_list">Loan List</a></li>
                    </ul></li>

                <li><a href="#">Insurance</a>
                    <ul>
                    <li><a href="claim_insurance">Claim Insurance (New)</a></li>
                    <li><a href="insurance_list">View Insurance</a></li>
                    </ul>
                </li>
                <li><a href="#">IT Deduction</a>
                    <ul>
                    <li><a href="it_declaration">IT Declaration</a></li>
                    <li><a href="it_declaration_list">IT List</a></li>
                    </ul>
                </li>
                <li><a href="#">Employee Attendance</a>
                    <ul>
                    <li><a href="#">Attendance</a></li>
                    <li><a href="#">View Attendance</a></li>
                    </ul>
                </li>
                <li><a href="#">Employee Payroll</a>
                    <ul>
                    <li><a href="employee_salary">Add Payslip</a></li>
                    <li><a href="#">List Payments</a></li>
                    </ul>
                </li>
                <li><a href="#">Seperation</a>
                    <ul>
                    <li><a href="initiate_resignation">Initiate Resignation</a></li>
                    <li><a href="resignation_acceptance">Resignation Acceptance</a></li>
                    <li><a href="final_settlement">Full & Final Settlement</a></li>
                    </ul>
                </li>
              </ul>
            </li>
            <li>
                <a href="#"><i class="zmdi zmdi-view-dashboard"></i><span class="list-label">Performance</span></a>
                <ul>
                    <li><a href="#">Performance Management</a>
                    <ul>
                    <li><a href="set_goals">Set Goals</a></li>
                    <li><a href="update_performance">Update Performance</a></li>
                    <li><a href="performance_review">Performance Review</a></li>
                    <li><a href="#">History Information</a></li>
                    </ul></li>
                </ul>
            </li>
        </ul>
    </div>
</aside>
<!--Leftbar End Here-->
<!--Page Container Start Here-->
<section class="main-container">
<div class="container-fluid">
<div class="page-header filled full-block light">
    <div class="row">
        <div class="col-md-6 col-sm-6">
            <h2>View Products</h2>
            </div>
        <div class="col-md-6 col-sm-6">
            <ul class="list-page-breadcrumb">
                <li><a href="#">Home <i class="zmdi zmdi-chevron-right"></i></a></li>
                <li><a href="#">Master <i class="zmdi zmdi-chevron-right"></i></a></li>
                <li><a href="#">Product <i class="zmdi zmdi-chevron-right"></i></a></li>
                <li class="active-page">View Product</li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
        <div class="col-md-12">
            <div class="widget-wrap">
                <div class="widget-header block-header clearfix">
                    <h3>Product List</h3>
                    <!--p>
                        Use <code>class="table data-tbl"</code> for this table styles
                    </p-->
                </div>
    
          <div class="clearfix"></div>
                <div class="widget-container">
                    <div class="widget-content">
                            <div class="row">
                                    <div class="col-md-6 unit">
                                    <div class="col-md-3 j-forms j-custom"> 
                                        <label class="label">Total Entries</label>
                                        </div>
                                        <div class="col-md-9"> 
                                        <div class="input">
                                                <select>
                                                        <option value="10">10</option>
                                                        <option value="20">20</option>
                                                        <option value="30">30</option>
                                                        <option value="40">40</option>
                                                        <option value="50">50</option>
                                                    </select>
                                              </div> </div>
                                    </div>
                                    <div class="col-md-6 unit">
                                    <div class="col-md-3 j-forms j-custom"> 
                                        <label class="label">search projects:</label></div>
                                        <div class="col-md-9"> 
                                        <div class="input">
                
                                            <input type="search" class="required-group form-control" required="" name="part_number" id="part_number">
                                        </div></div>
                                        
                                    </div>
                                </div>
                                
                        <table class="table table-striped data-tbl">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Product Name</th>
                                <th>Part Number</th>
                                <th>ProductActive</th>
                                <th>Product Category</th>
                                <th>Sales Start Date</th>
                                <th>Sales End Date</th>
                                <th>SupportStartDate</th>
                                <th>SupportEndDate</th>
                                <th>SerialNo</th>
                                <th>MfrPartNo</th>
                                <th>Manufacturer</th>
                                <th>Website</th>
                                <th>GlAccount</th>
                                <th>ProductSheet</th>
                                <th>Unit Price</th>
                                <th>Cgst</th>
                                <th>Sgst</th>
                                <th>Igst</th>
                                <th>PurchaseCost</th>
                                <th>UsageUnit</th>
                                <th>QtyUnit</th>
                                <th>QtyinStock</th>
                                <th>reorderlevel</th>
                                <th>Handler</th>
                                <th>QtyinDemand</th>
                                <th>Description</th>
                                

                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            include './db.php';
                                $serial='1';
                                $EnquiryQuery = "SELECT * FROM product";      
                                $EnquiryResult = mysqli_query($conn,$EnquiryQuery) or die(mysqli_error($conn));   
                                        
                                  
                                 while ($result = mysqli_fetch_array($EnquiryResult))
                                 {  
                                  
                                      
                                ?>
    
                <tr>
                                        <td><?php echo $serial; ?></td>
                                        <td><?php echo $result['ProductName']; ?></td>
                                        <td><?php echo $result['PartNumber']; ?></td>
                                        <td><?php echo $result['ProductActive']; ?></td>
                                        <td><?php echo $result['ProductCategory']; ?></td>
                                        <td><?php echo $result['SalesStartdate']; ?></td>
                                        <td><?php echo $result['SalesEndDate']; ?></td>
                                        <td><?php echo $result['SupportStartDate']; ?></td>
                                        <td><?php echo $result['SupportEndDate']; ?></td>
                                        <td><?php echo $result['SerialNo']; ?></td>
                                        <td><?php echo $result['MfrPartNo']; ?></td>
                                        <td><?php echo $result['Manufacturer']; ?></td>
                                        <td><?php echo $result['Website']; ?></td>
                                        <td><?php echo $result['GlAccount']; ?></td>
                                        <td><?php echo $result['ProductSheet']; ?></td>
                                        <td><?php echo $result['Unitprice']; ?></td>
                                        <td><?php echo $result['Cgst']; ?></td>
                                        <td><?php echo $result['Sgst']; ?></td>
                                        <td><?php echo $result['Igst']; ?></td>
                                        <td><?php echo $result['PurchaseCost']; ?></td>
                                        <td><?php echo $result['UsageUnit']; ?></td>
                                        <td><?php echo $result['QtyUnit']; ?></td>
                                        <td><?php echo $result['QtyinStock']; ?></td>
                                        <td><?php echo $result['reorderlevel']; ?></td>
                                        <td><?php echo $result['Handler']; ?></td>
                                        <td><?php echo $result['QtyinDemand']; ?></td>
                                        <td><?php echo $result['Description']; ?></td>

                                        <td><a href="edit-product.php"><button class="btn btn-sm btn-primary">edit</button></a><button class="btn btn-sm btn-danger" data-toggle="modal" data-target="#myModal">delete</button><button class="btn btn-sm btn-info">more</button>
										<div id="myModal" class="modal fade" role="dialog">
                                                                            <div class="modal-dialog">
                                                                          
                                                                              <!-- Modal content-->
                                                                              <div class="modal-content">
                                                                                <div class="modal-header">
                                                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                                  <h4 class="modal-title text-center">Delete Product</h4>
                                                                                </div>
                                                                                <div class="modal-body text-center">
                                                                                  <p>Are your sure?</p>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-info text-center">Cancel</button>
                                                                                  <button type="button" class="btn btn-danger">Yes, Continue</button>
                                                                                </div>
                                                                              </div>
                                                                          
                                                                            </div>
                                                                          </div></td>
                                        </tr>
                                        <?php
                                 $serial++;
                                 }
                                 ?>
                            </tbody>
                        </table>
                        <div class="row">
                                <div class="col-md-6">
                                    <div class="">showing 1 to 10 of 12 entries</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="pull-right">
                                        <ul class="pagination pagination-custom">
                                                <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                                <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        </ul>
                                    </div>
                                </div>
                                </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Footer Start Here -->
<footer class="footer-container">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <div class="footer-left">
                    <span>© 2019 <a href="http://inscomi.com">inscomi</a></span>
                </div>
            </div>
            <div class="col-md-6 col-sm-6">
                <div class="footer-right">
                    <span class="footer-meta">Crafted with&nbsp;<i class="fa fa-heart"></i>&nbsp;by&nbsp;<a href="http://inscomi.com">inscomi</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--Footer End Here -->
</section>
<!--Page Container End Here-->
<!--Rightbar Start Here-->
<!--Rightbar End Here-->
<script src="js/lib/jquery.js"></script>
<script src="js/lib/jquery-migrate.js"></script>
<script src="js/lib/bootstrap.js"></script>
<script src="js/lib/jquery.ui.js"></script>
<script src="js/lib/jRespond.js"></script>
<script src="js/lib/nav.accordion.js"></script>
<script src="js/lib/hover.intent.js"></script>
<script src="js/lib/hammerjs.js"></script>
<script src="js/lib/jquery.hammer.js"></script>
<script src="js/lib/jquery.fitvids.js"></script>
<script src="js/lib/scrollup.js"></script>
<script src="js/lib/smoothscroll.js"></script>
<script src="js/lib/jquery.slimscroll.js"></script>
<script src="js/lib/jquery.syntaxhighlighter.js"></script>
<script src="js/lib/velocity.js"></script>
<script src="js/lib/jquery-jvectormap.js"></script>
<script src="js/lib/jquery-jvectormap-world-mill.js"></script>
<script src="js/lib/jquery-jvectormap-us-aea.js"></script>
<script src="js/lib/smart-resize.js"></script>
<!--iCheck-->
<script src="js/lib/icheck.js"></script>
<script src="js/lib/jquery.switch.button.js"></script>
<!--CHARTS-->
<script src="js/lib/chart/sparkline/jquery.sparkline.js"></script>
<script src="js/lib/chart/easypie/jquery.easypiechart.min.js"></script>
<script src="js/lib/chart/flot/excanvas.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.min.js"></script>
<script src="js/lib/chart/flot/curvedLines.js"></script>
<script src="js/lib/chart/flot/jquery.flot.time.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.stack.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.axislabels.js"></script>
<script src="js/lib/chart/flot/jquery.flot.resize.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.tooltip.min.js"></script>
<script src="js/lib/chart/flot/jquery.flot.spline.js"></script>
<script src="js/lib/chart/flot/jquery.flot.pie.min.js"></script>
<!--Forms-->
<script src="js/lib/jquery.maskedinput.js"></script>
<script src="js/lib/jquery.validate.js"></script>
<script src="js/lib/jquery.form.js"></script>
<script src="js/lib/j-forms.js"></script>
<script src="js/lib/jquery.loadmask.js"></script>
<script src="js/lib/vmap.init.js"></script>
<script src="js/lib/theme-switcher.js"></script>
<script src="js/apps.js"></script>
</body>

</html>